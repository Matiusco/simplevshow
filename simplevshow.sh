#!/usr/bin/env bash
#-----------------------------------------------------------------------#
# file original			: streamcrunchyroll.sh
# 						: https://tinyurl.com/mulin-streamcrunchyroll
# Credits dev			: @Mulin
# Blog Educador Livre	: www.blogdomulin.com.br
#-----------------------------------------------------------------------#
# https://t.me/cursognu
# https://t.me/debxpcomunidade
#-----------------------------------------------------------------------#
##	NOME			: simplevshow.sh
##	VERSÃO			: 0.7 alfa 
##	DESCRIÇÃO		: Baixa/Salva videos youtube e mostra sem propaganda.
##	DATA DA CRIAÇÃO	: 28/08/2021
##	DEV APRENDIZ	: Matheus O. Couto aka @Matiusco 
##	E-MAIL			: matius.couto@gmail.com
##	BETA-TESTS		: Só use pra testar. Mande o nome no @curnognu
##	DISTRO			: Debian GNU/Linux 11
##	LICENÇA			: GPLv3+
##	PROJETO			: https://codeberg.org/Matiusco/simplevshow
#-----------------------------------------------------------------------#
#	MENSAGEM DO DIA: #FORABOLSONAROGENOCIDA
#-----------------------------------------------------------------------#
# DEPENDÊNCIAS
#
#	- youtube-dl
#	- mpv
#
#	- debian 
#-----------------------------------------------------------------------#

#-----------------------------------------------------------------------#
# VARIÁVEIS GLOBAIS #####################################################
#-----------------------------------------------------------------------#
distro=$(grep ^ID= /etc/*-release | cut -d= -f2)

declare -A msg_svs
#-----------------------------------------------------------------------#
# FUNÇÕES ###############################################################
#-----------------------------------------------------------------------#


# Verificar dependencias.
# Tem potencial para virar utilitário genérico. 
#-----------------------------------------------------------------------#
chk_dependencias ()
{
	[[ $distro != 'debian' ]] && echo "Você precisa estar no Debian..., abortando..." && exit 1
	[[ -z $(command -v youtube-dl) ]] && echo "Instalar antes youtube-dl , abortando..." && exit 1
	[[ -z $(command -v mpv) ]] && echo "Instalar antes mpv , abortando..." && exit 1
}

# Verificar a existência do arq de configuração. 
#-----------------------------------------------------------------------#
chk_file_config ()
{
	# Se não existe vai criar vazio.
	# Sempre no mesmo diretório da instalação. 
	if [ ! -f "$file_config" ] 
	then 
		echo 'Arquivo de configuração será criado'
		echo "Vou gravar o log aqui" 
		>"$file_config"
		return 1
	else
		echo "Arquivo de configuração $file_config já existe na função. "
		echo "Será carregado aqui as configs...."
		return 0		
	fi	
}


saindo_ok ()
{
	# Apaga qualquer arquivo criado pelo script no tmp.
	rm -rf /tmp/simplevshow-script.tmp.*
}

# Criando temporário de trabalho durante a sessão. 
#-----------------------------------------------------------------------#
tmpfile_config=$(mktemp /tmp/simplevshow-script.tmp.XXXXXXXXXXXXXX)

# Lembrar de remover #RemoverLinha
#-----------------------------------------------------------------------#
echo $tmpfile_config

# Logs do script
#-----------------------------------------------------------------------#
file_log="simplevshow.log"

# Arquivo de configuração
#-----------------------------------------------------------------------#
file_config="simplevshow.config"


# Lembrar de remover #RemoverLinha
#-----------------------------------------------------------------------#
echo "Antes de entrar na chamada da função"


# Verifica denpendências e sai se não atendida.
# TODO: No futuro função vai tentar instalar dependência.
#-----------------------------------------------------------------------#
chk_dependencias

# Testa se existe arquivo de configuração
# Cria Vazio se não houver. Depois vai colocar as configs.
#-----------------------------------------------------------------------#
chk_file_config
echo $?


# Lembrar de remover #RemoverLinha
#-----------------------------------------------------------------------#
echo "Arquivo de configuração ${file_config} depois da declração"

 
# Lembrar de remover #RemoverLinha
#-----------------------------------------------------------------------#
sleep 5


tmp_formats=$(mktemp /tmp/simplevshow-script.tmp.XXXXXXXXXXXXXX)


clear

echo "Cole o link do episódio que deseja assistir (Ctrl+Shift+V): "
echo "Exemplo:  https://youtu.be/d1-f93GAI48 "
read link

# Gera arquivo com formatos possivéis. Só com áudio e vídeo.
#-----------------------------------------------------------------------#
youtube-dl -F $link | sed '1,4d' | grep 'p ' | cut -c 36-38 | sort | uniq >"$tmp_formats"



select opt in $(cat $tmp_formats) ; do
    #echo "Opção escolhida: $opt"
    break
done

# Aqu vai mudar para codigo de video
#-----------------------------------------------------------------------#
qualidade=$opt

# Aqui vai ter alteração pra pegar o código do vídeo.
#-----------------------------------------------------------------------#
aa="mpv $link --slang=ptBR --ytdl-format=[height=$qualidade]"


# Em breve vamos usar o vídeo salvo pra mostrar.
# Se assim o confg estiver configurado.
#-----------------------------------------------------------------------#
${aa} 2>/dev/null 1>/dev/null 


# Limpa todos temporários.
#-----------------------------------------------------------------------#
saindo_ok 

exit


# Importante.
# detectar o ambiente de área de trabalho no Linux. XFCE por exemplo.
#echo $XDG_CURRENT_DESKTOP


# Lembrar de remover #RemoverLinha
# link de exemplo
# use este vídeo pra testar.... ou não kkkk
link='https://youtu.be/d1-f93GAI48'
link='https://youtu.be/SZMIL87CyVE'
link='https://youtu.be/GOK1OzdJAKU'

#TODO
# Permitir acesso a playlist para salvar local.
# Poderá ficar salvando em background.
playlist-link='https://www.youtube.com/playlist?list=PLuf64C8sPVT8nFC5s-UZ4gJQSEyvEjvhS'
